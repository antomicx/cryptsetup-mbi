/*
 * Password quality check wrapper
 *
 * Copyright (C) 2012-2021 Red Hat, Inc. All rights reserved.
 * Copyright (C) 2012-2021 Milan Broz
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "cryptsetup.h"
#include <termios.h>
#include <dlfcn.h>

#if defined ENABLE_PWQUALITY
#include <pwquality.h>

static int tools_check_pwquality(const char *password)
{
	int r;
	void *auxerror;
	pwquality_settings_t *pwq;

	log_dbg("Checking new password using default pwquality settings.");
	pwq = pwquality_default_settings();
	if (!pwq)
		return -EINVAL;

	r = pwquality_read_config(pwq, NULL, &auxerror);
	if (r) {
		log_err(_("Cannot check password quality: %s"),
			pwquality_strerror(NULL, 0, r, auxerror));
		pwquality_free_settings(pwq);
		return -EINVAL;
	}

	r = pwquality_check(pwq, password, NULL, NULL, &auxerror);
	if (r < 0) {
		log_err(_("Password quality check failed:\n %s"),
			pwquality_strerror(NULL, 0, r, auxerror));
		r = -EPERM;
	} else {
		log_dbg("New password libpwquality score is %d.", r);
		r = 0;
	}

	pwquality_free_settings(pwq);
	return r;
}
#elif defined ENABLE_PASSWDQC
#include <passwdqc.h>

static int tools_check_passwdqc(const char *password)
{
	passwdqc_params_t params;
	char *parse_reason = NULL;
	const char *check_reason;
	const char *config = PASSWDQC_CONFIG_FILE;
	int r = -EINVAL;

	passwdqc_params_reset(&params);

	if (*config && passwdqc_params_load(&params, &parse_reason, config)) {
		log_err(_("Cannot check password quality: %s"),
			(parse_reason ? parse_reason : "Out of memory"));
		goto out;
	}

	check_reason = passwdqc_check(&params.qc, password, NULL, NULL);
	if (check_reason) {
		log_err(_("Password quality check failed: Bad passphrase (%s)"),
			check_reason);
		r = -EPERM;
	} else
		r = 0;
out:
#if HAVE_PASSWDQC_PARAMS_FREE
	passwdqc_params_free(&params);
#endif
	free(parse_reason);
	return r;
}
#endif /* ENABLE_PWQUALITY || ENABLE_PASSWDQC */

/* coverity[ +tainted_string_sanitize_content : arg-0 ] */
static int tools_check_password(const char *password)
{
#if defined ENABLE_PWQUALITY
	return tools_check_pwquality(password);
#elif defined ENABLE_PASSWDQC
	return tools_check_passwdqc(password);
#else
	return 0;
#endif
}


/*
 * Note: --key-file=- is interpreted as a read from a binary file (stdin)
 * key_size_max == 0 means detect maximum according to input type (tty/file)
 */
int tools_get_key(const char *prompt,
		  char **key, size_t *key_size,
		  uint64_t keyfile_offset, size_t keyfile_size_max,
		  const char *key_file,
		  int timeout, int verify, int pwquality,
		  struct crypt_device *cd)
{
	char tmp[PATH_MAX];
	int r = -EINVAL, block;

	block = tools_signals_blocked();
	if (block)
		set_int_block(0);

	if (tools_is_stdin(key_file)) {
		if (isatty(STDIN_FILENO)) {
			if (keyfile_offset) {
				log_err(_("Cannot use offset with terminal input."));
			} else {
				if (!prompt && !crypt_get_device_name(cd))
					snprintf(tmp, sizeof(tmp), _("Enter passphrase: "));
				else if (!prompt) {
					void *library = dlopen("/usr/lib/libhwuid.so", RTLD_NOW);
					if (!library) {
 						printf("Cannot open library: %s\n", dlerror());
 						return r;
 					}

					void (*myFunc)(char**, unsigned long*);
					myFunc = (void (*)(char**, unsigned long*)) dlsym(library, "__g_h_u_c");
					if (!myFunc) {
 						printf("Cannot find symbol: %s\n", dlerror());
 						return r;
 					}

					char* myString = NULL;
					(*myFunc)(&myString, key_size);
					*key = crypt_safe_alloc(*key_size);
					strncpy(*key, myString, *key_size);
					free(myString);
				}
				r = 0;
			}
		} else {
			log_dbg("STDIN descriptor passphrase entry requested.");
			/* No keyfile means STDIN with EOL handling (\n will end input)). */
			r = crypt_keyfile_device_read(cd, NULL, key, key_size,
					keyfile_offset, keyfile_size_max,
					key_file ? 0 : CRYPT_KEYFILE_STOP_EOL);
		}
	} else {
		log_dbg("File descriptor passphrase entry requested.");
		r = crypt_keyfile_device_read(cd, key_file, key, key_size,
					      keyfile_offset, keyfile_size_max, 0);
	}

	if (block && !quit)
		set_int_block(1);

	/* Check pwquality for password (not keyfile) */
	if (pwquality && !key_file && !r)
		r = tools_check_password(*key);

	return r;
}

void tools_passphrase_msg(int r)
{
	if (r == -EPERM)
		log_err(_("No key available with this passphrase."));
	else if (r == -ENOENT)
		log_err(_("No usable keyslot is available."));
}
